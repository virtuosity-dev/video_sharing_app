describe("The App", () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it("is able to search and select a video", () => {
    const searchTerm = "javascript";
    const regex = new RegExp(searchTerm, "i");
    cy.get("[data-qa=search-input]")
      .type(searchTerm)
      .should("have.value", searchTerm);

    cy.get("[data-qa=search-button]")
      .click();
    cy.get("[data-qa=video-item]")
      .should("have.length", 5);

    cy.get("[data-qa=video-item]")
      .first()
      .within(() => {
        cy.get("[data-qa=video-item-title]")
          .should(($title) => {
            expect($title.text()).match(regex);
          });
      });

    cy.get("[data-qa=video-item]")
      .first()
      .within(() => {
        cy.get("[data-qa=video-item-title]")
          .as("video-item-title")
      })
      .click();
    cy.get("[data-qa=video-iframe]")
      .should("exist");
    cy.get("[data-qa=video-title]")
      .then(($title) => {
        cy.get("@video-item-title")
          .should(($itemTitle) => {
            expect($title.text()).to.eql($itemTitle.text());
          });
      });
    cy.get("[data-qa=video-desc]")
      .should("exist");
  });
});