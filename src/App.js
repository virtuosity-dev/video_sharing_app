
import React from 'react';
import './App.css';
import './App.sass';
import SearchBar from './containers/SearchBar';
import {searchVideos} from './apis/youtube';
import VideoList from './containers/VideoList';
import VideoDetail from './containers/VideoDetail';
import {logActivity} from './utils/helpers';

class App extends React.Component {
  state = {
    videos: [],
    selectedVideo: null
  };
  handleSubmit = (searchFunction) => async (termFromSearchBar) => {
    logActivity(`User searched for keyword ${termFromSearchBar}`);
    try {
      const data = await searchFunction(termFromSearchBar);
      this.setState({
        videos: data.items
      });
    } catch(e) {}
  };
  handleVideoSelect = (video) => {
    logActivity(`User selected video ${video}`);
    this.setState({selectedVideo: video});
  };

  render() {
    return (
      <div className="container">
        <div className="columns">
          <div className="column">
            <SearchBar handleFormSubmit={this.handleSubmit(searchVideos)}/>
          </div>
        </div>
        <div className="columns">
          <div className="column is-two-thirds">
            <VideoDetail video={this.state.selectedVideo}/>
          </div>
          <div className="column is-one-third">
            <VideoList handleVideoSelect={this.handleVideoSelect} videos={this.state.videos}/>
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <button
              onClick={() => {
                console.error("Unknown User killed the app.");
                throw new Error("\"Throw Error\" button clicked");
              }}
            >Throw Error</button>
          </div>
        </div>
      </div>
    );
  };
}

export default App;
