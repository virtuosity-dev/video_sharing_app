//Sample data for tests
export default {
  "kind": "youtube#searchListResponse",
  "etag": "\"p4VTdlkQv3HQeTEaXgvLePAydmU/EyYFLWu_05LXKFSLKB8mHonLPDg\"",
  "nextPageToken": "CAUQAA",
  "regionCode": "MY",
  "pageInfo": {
    "totalResults": 1000000,
    "resultsPerPage": 5
  },
  "items": [
    {
    "kind": "youtube#searchResult",
    "etag": "\"p4VTdlkQv3HQeTEaXgvLePAydmU/RioraahwQ52jG-2lI8yJxQbpe38\"",
    "id": {
      "kind": "youtube#video",
      "videoId": "zAGVQLHvwOY"
    },
    "snippet": {
      "publishedAt": "2019-08-28T15:59:43.000Z",
      "channelId": "UCjmJDM5pRKbUlVIzDYYWb6g",
      "title": "JOKER - Final Trailer - Now Playing In Theaters",
      "description": "https://www.joker.movie https://www.facebook.com/jokermovie https://twitter.com/jokermovie https://www.instagram.com/jokermovie/ Director Todd Phillips ...",
      "thumbnails": {
      "default": {
        "url": "https://i.ytimg.com/vi/zAGVQLHvwOY/default.jpg",
        "width": 120,
        "height": 90
      },
      "medium": {
        "url": "https://i.ytimg.com/vi/zAGVQLHvwOY/mqdefault.jpg",
        "width": 320,
        "height": 180
      },
      "high": {
        "url": "https://i.ytimg.com/vi/zAGVQLHvwOY/hqdefault.jpg",
        "width": 480,
        "height": 360
      }
      },
      "channelTitle": "Warner Bros. Pictures",
      "liveBroadcastContent": "none"
    }
    },
    {
    "kind": "youtube#searchResult",
    "etag": "\"p4VTdlkQv3HQeTEaXgvLePAydmU/yrKm7s0lHJuHz-RtjlDCVGinoEk\"",
    "id": {
      "kind": "youtube#video",
      "videoId": "QntBkDFkiuY"
    },
    "snippet": {
      "publishedAt": "2019-09-20T21:00:03.000Z",
      "channelId": "UCp0hYYBW6IMayGgR-WeoCvQ",
      "title": "Will Smith Surprises Viral Video Classmates for Their Kindness",
      "description": "Ellen welcomed high school students Kristopher and Antwain, who surprised their bullied classmate, Micheal, with new clothes and shoes. A video of the kind ...",
      "thumbnails": {
      "default": {
        "url": "https://i.ytimg.com/vi/QntBkDFkiuY/default.jpg",
        "width": 120,
        "height": 90
      },
      "medium": {
        "url": "https://i.ytimg.com/vi/QntBkDFkiuY/mqdefault.jpg",
        "width": 320,
        "height": 180
      },
      "high": {
        "url": "https://i.ytimg.com/vi/QntBkDFkiuY/hqdefault.jpg",
        "width": 480,
        "height": 360
      }
      },
      "channelTitle": "TheEllenShow",
      "liveBroadcastContent": "none"
    }
    },
    {
    "kind": "youtube#searchResult",
    "etag": "\"p4VTdlkQv3HQeTEaXgvLePAydmU/0kOj36CP_A_SVpQNhV-FoKa8yW8\"",
    "id": {
      "kind": "youtube#video",
      "videoId": "FHYtj1m_5QI"
    },
    "snippet": {
      "publishedAt": "2018-05-19T13:00:00.000Z",
      "channelId": "UCp0hYYBW6IMayGgR-WeoCvQ",
      "title": "Ellen Shocks Jeannie With a Huge Surprise",
      "description": "It's been 10 years since Ellen gave Jeannie her first-ever job, and to celebrate the milestone, Ellen shocked Jeannie with an unforgettable surprise.",
      "thumbnails": {
      "default": {
        "url": "https://i.ytimg.com/vi/FHYtj1m_5QI/default.jpg",
        "width": 120,
        "height": 90
      },
      "medium": {
        "url": "https://i.ytimg.com/vi/FHYtj1m_5QI/mqdefault.jpg",
        "width": 320,
        "height": 180
      },
      "high": {
        "url": "https://i.ytimg.com/vi/FHYtj1m_5QI/hqdefault.jpg",
        "width": 480,
        "height": 360
      }
      },
      "channelTitle": "TheEllenShow",
      "liveBroadcastContent": "none"
    }
    },
    {
    "kind": "youtube#searchResult",
    "etag": "\"p4VTdlkQv3HQeTEaXgvLePAydmU/vMauRp7ZK_Zdx93Xv6KiglTSgj4\"",
    "id": {
      "kind": "youtube#video",
      "videoId": "29a6o5vRKVM"
    },
    "snippet": {
      "publishedAt": "2019-07-18T16:12:43.000Z",
      "channelId": "UCEdvpU2pFRCVqU6yIPyTpMQ",
      "title": "Marshmello &amp; Kane Brown - One Thing Right (Official Music Video)",
      "description": "Marshmello & Kane Brown - One Thing Right (Official Music Video) Download / Stream 'One Thing Right' ▷ https://smarturl.it/melloxkb Shop The Official 'One ...",
      "thumbnails": {
      "default": {
        "url": "https://i.ytimg.com/vi/29a6o5vRKVM/default.jpg",
        "width": 120,
        "height": 90
      },
      "medium": {
        "url": "https://i.ytimg.com/vi/29a6o5vRKVM/mqdefault.jpg",
        "width": 320,
        "height": 180
      },
      "high": {
        "url": "https://i.ytimg.com/vi/29a6o5vRKVM/hqdefault.jpg",
        "width": 480,
        "height": 360
      }
      },
      "channelTitle": "Marshmello",
      "liveBroadcastContent": "none"
    }
    },
    {
    "kind": "youtube#searchResult",
    "etag": "\"p4VTdlkQv3HQeTEaXgvLePAydmU/Wvbj-dIMfJqPv2tvGAfLzx4HnCo\"",
    "id": {
      "kind": "youtube#video",
      "videoId": "w2Ov5jzm3j8"
    },
    "snippet": {
      "publishedAt": "2019-05-17T15:57:45.000Z",
      "channelId": "UCtTfSyci2urfwXXu_eRpNRA",
      "title": "Lil Nas X - Old Town Road (Official Movie) ft. Billy Ray Cyrus",
      "description": "Official video for Lil Nas X's Billboard #1 hit, “Old Town Road (Remix)” featuring Billy Ray Cyrus. Special guest appearances from Chris Rock, Haha Davis, Rico ...",
      "thumbnails": {
      "default": {
        "url": "https://i.ytimg.com/vi/w2Ov5jzm3j8/default.jpg",
        "width": 120,
        "height": 90
      },
      "medium": {
        "url": "https://i.ytimg.com/vi/w2Ov5jzm3j8/mqdefault.jpg",
        "width": 320,
        "height": 180
      },
      "high": {
        "url": "https://i.ytimg.com/vi/w2Ov5jzm3j8/hqdefault.jpg",
        "width": 480,
        "height": 360
      }
      },
      "channelTitle": "LilNasXVEVO",
      "liveBroadcastContent": "none"
    }
    }
  ]
};
