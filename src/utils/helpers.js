import { Timber } from "@timberio/node";

var logger = process.env.NODE_ENV === 'production'
  ? new Timber(process.env.REACT_APP_TIMBER_API_KEY, process.env.REACT_APP_TIMBER_SOURCE_ID)
  : undefined;

export const decodeHtml = (html) => {
  var txt = document.createElement("textarea");
  txt.innerHTML = html;
  return txt.value;
};

export const logActivity = (message) => {
  if(process.env.NODE_ENV === 'production') {
    logger.info(
      `[VideoSharingApp] ${message}`,
      {user: 'Unknown'}
    ).then(log => {
      console.log(log)
    });
  } else {
    console.log(message);
  }
}
