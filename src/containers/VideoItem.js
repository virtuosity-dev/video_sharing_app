import React from 'react';
import {decodeHtml} from '../utils/helpers';

const VideoItem = ({video , handleVideoSelect}) => {
  return (
    <div className="media" onClick={() => handleVideoSelect(video)} data-qa="video-item">
      <figure className="media-left">
        <figure className="image" style={{width: 128, height: 72}}>
          <img src={video.snippet.thumbnails.medium.url} alt={video.snippet.description} data-qa="video-item-image"/>
        </figure>
      </figure>
      <div className="media-content">
        <div className="content">
          <p data-qa="video-item-title"><strong>{decodeHtml(video.snippet.title)}</strong></p>
          <p data-qa="video-item-channel">{decodeHtml(video.snippet.channelTitle)}</p>
        </div>
      </div>
    </div>
  );
};

export default VideoItem;
