import React from 'react';
import {shallow} from 'enzyme';
import VideoDetail from '../VideoDetail';
import sampleResults from '../../utils/sampleResults';

const sampleVideo = sampleResults.items[0];

describe("Test VideoDetail component", () => {
  it("renders without a video without crashing", () => {
    shallow(<VideoDetail/>);
  });

  it("renders with a video without crashing", () => {
    shallow(<VideoDetail video={sampleVideo}/>);
  });

  it("displays \"Loading...\" when nothing is selected", () => {
    const wrapper = shallow(<VideoDetail/>);
    expect(wrapper.text()).toEqual("Loading ...");
    expect(wrapper.exists('[data-qa="video-iframe"]')).toBeFalsy();
    expect(wrapper.exists('[data-qa="video-title"]')).toBeFalsy();
    expect(wrapper.exists('[data-qa="video-desc"]')).toBeFalsy();
  });

  it("displays the video, its title and description when it is selected", () => {
    const wrapper = shallow(<VideoDetail video={sampleVideo}/>);
    expect(wrapper.text()).not.toEqual("Loading ...");
    expect(wrapper.exists('[data-qa="video-iframe"]')).toBeTruthy();
    expect(wrapper.exists('[data-qa="video-title"]')).toBeTruthy();
    expect(wrapper.exists('[data-qa="video-desc"]')).toBeTruthy();
  });
})