import React from "react";
import {shallow} from "enzyme";
import SearchBar from "../SearchBar";

describe("Test SearchBar component", () => {
  it("renders without crashing", () => {
    shallow(<SearchBar handleFormSubmit={() => {}}/>);
  });

  it("contains a Search Input", () => {
    const wrapper = shallow(<SearchBar handleFormSubmit={()=>{}}/>);
    expect(wrapper.exists('[data-qa="search-input"]')).toBeTruthy();
    expect(wrapper.find('[data-qa="search-input"]').props().placeholder).toBe("Search title...");
  });

  it("contains a Search Button", () => {
    const wrapper = shallow(<SearchBar handleFormSubmit={()=>{}}/>);
    expect(wrapper.exists('[data-qa="search-button"]')).toBeTruthy();
    expect(wrapper.find('[data-qa="search-button"]').first().props().type).toBe("submit");
    expect(wrapper.find('[data-qa="search-button"]').first().props().children).toBe("Search");
  });

  it("handles \"handleFormSubmit\" function correctly when form is submitted", () => {
    const fnSubmit = jest.fn();
    const wrapper = shallow(<SearchBar handleFormSubmit={fnSubmit}/>);
    wrapper.find('[data-qa="search-form"]').simulate('submit', { preventDefault () {} });
    expect(fnSubmit).toHaveBeenCalled();
  });
});
