import React from 'react';
import {shallow} from 'enzyme';
import VideoItem from '../VideoItem';
import sampleResults from '../../utils/sampleResults';

const sampleVideo = sampleResults.items[0];

describe("Test VideoItem component", () => {
  it("renders with a video without crashing", () => {
    shallow(<VideoItem video={sampleVideo} handleVideoSelect={()=>{}}/>);
  });

  it("renders with the video's image, title, and channel", () => {
    const wrapper = shallow(<VideoItem video={sampleVideo} handleVideoSelect={()=>{}}/>);
    expect(wrapper.exists('[data-qa="video-item-image"]')).toBeTruthy();
    expect(wrapper.exists('[data-qa="video-item-title"]')).toBeTruthy();
    expect(wrapper.exists('[data-qa="video-item-channel"]')).toBeTruthy();
  });

  it("handles \"handleVideoSelect\" function correctly when clicked", () => {
    const fnClick = jest.fn();
    const wrapper = shallow(<VideoItem video={sampleVideo} handleVideoSelect={fnClick}/>);
    wrapper.find('[data-qa="video-item"]').simulate('click');
    expect(fnClick).toHaveBeenCalled();
  });
});