import React from 'react';
import VideoItem from './VideoItem';

const VideoList = ({videos , handleVideoSelect}) => {
  const renderedVideos =  videos.map((video) => {
    return (
      <a className="panel-block" key={video.id.videoId}>
        <VideoItem video={video} handleVideoSelect={handleVideoSelect}/>
      </a>
    );
  });

  return <div className="panel" data-qa="video-list">{renderedVideos}</div>;
};

export default VideoList;
