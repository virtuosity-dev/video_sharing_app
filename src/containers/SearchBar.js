import React from 'react';

class SearchBar extends React.Component {
  state = {
    term: ""
  };
  handleChange = (event) => {
    this.setState({
      term: event.target.value
    });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    this.props.handleFormSubmit(this.state.term);
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit} data-qa="search-form">
        <div className="field has-addons">
          <div className="control" style={{width: '100%'}}>
            <input
              className="input"
              type="text"
              placeholder="Search title..."
              onChange={this.handleChange}
              value={this.state.term}
              data-qa="search-input"
            />
          </div>
          <div className="control">
            <button type="submit" className="button is-info" data-qa="search-button">
              Search
            </button>
          </div>
        </div>
      </form>
    );
  };
};

export default SearchBar;
