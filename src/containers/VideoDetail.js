import React from 'react';
import {decodeHtml} from '../utils/helpers';

const VideoDetail = ({video}) => {
  if (!video) {
    return <div>Loading ...</div>;
  }

  const videoSrc = `https://www.youtube.com/embed/${video.id.videoId}`;
  return (
    <div className="container">
      <div className="columns">
        <div className="column">
          <div className="video-container">
            <iframe width="640" height="360" src={videoSrc} allowFullScreen title='Video player' data-qa="video-iframe"/>
          </div>
        </div>
      </div>
      <div className="columns">
        <div className="column">
          <div className="content" data-qa="video-details">
            <h4 data-qa="video-title">{decodeHtml(video.snippet.title)}</h4>
            <p data-qa="video-desc">{decodeHtml(video.snippet.description)}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VideoDetail;
