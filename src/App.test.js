import React from 'react';
import {shallow} from 'enzyme';
import App from './App';
import sampleResults from './utils/sampleResults';
import {searchVideos} from './apis/youtube';

const sampleVideos = sampleResults.items;

describe("Test App component", () => {
  it("renders without crashing", () => {
    shallow(<App/>);
  });

  it("renders all required Components", () => {
    const wrapper = shallow(<App/>);
    expect(wrapper.exists('SearchBar')).toBeTruthy();
    expect(wrapper.exists('VideoDetail')).toBeTruthy();
    expect(wrapper.exists('VideoList')).toBeTruthy();
  });
});

describe("Test App component's \"handleVideoSelect\" function", () => {
  it("changes \"VideoDetail\"'s video", () => {
    const wrapper = shallow(<App/>);
    expect(wrapper.find('VideoDetail').props().video).toBeFalsy();
    wrapper.instance().handleVideoSelect(sampleVideos[0]);
    expect(wrapper.find('VideoDetail').props().video).toBe(sampleVideos[0]);
  });
});

describe("Test App component's \"handleSubmit\" function", () => {
  it("changes \"VideoList\"'s videos", async () => {
    const mockSearchFunction = (_) => sampleResults;
    const wrapper = shallow(<App/>);
    expect(wrapper.find('VideoList').props().videos).toHaveLength(0);
    await wrapper.instance().handleSubmit(mockSearchFunction)("");
    expect(wrapper.find('VideoList').props().videos).toBe(sampleResults.items);
  });

  it("updates App with a list of videos from Youtube", async () => {
    const wrapper = shallow(<App/>);
    expect(wrapper.state()).toHaveProperty("videos", []);
    await wrapper.instance().handleSubmit(searchVideos)("");
    expect(wrapper.state().videos).not.toHaveLength(0);
    expect(wrapper.state().videos[0]).toHaveProperty(["id", "kind"], "youtube#video");
    expect(wrapper.state().videos[0]).toHaveProperty("snippet");
  });
});
