import {searchVideos} from '../youtube';

describe("Test \"searchVideos\" function", () => {
  it("returns 5 items from \"youtube#searchListResponse\"", async () => {
    const data = await searchVideos("");
    expect(data).toHaveProperty("kind", "youtube#searchListResponse");
    expect(data).toHaveProperty("items");
    expect(data.items).toHaveLength(5);
  });
});
