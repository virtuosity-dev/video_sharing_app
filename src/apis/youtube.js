import axios from 'axios';
const KEY = process.env.REACT_APP_YOUTUBE_API_KEY;

const axiosInstance = axios.create({
  baseURL: "https://www.googleapis.com/youtube/v3/",
  params: {
      part: 'snippet',
      maxResults: 5,
      key: KEY
  }
});

export const searchVideos = (searchTerm) => {
  return axiosInstance.get('/search', {
    params: {
      q: searchTerm
    }
  })
  .then(response => {
    return response.data;
  })
  .catch(error => {
    console.error(error);
  });
};
